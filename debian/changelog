ruby-fog-aws (3.12.0-2) unstable; urgency=medium

  * Team upload.
  * d/control (Depends): Remove interpreters and use ${ruby:Depends}.
  * d/ruby-tests.rake: Execute shindont with the correct Ruby binary
    (closes: #998480).
  * d/source/include-binaries: Remove unused file.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 12 Nov 2021 00:27:53 +0100

ruby-fog-aws (3.12.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Pirate Praveen ]
  * New upstream version 3.12.0
  * Refresh patches and drop patches no longer required
  * Add ruby-simplecov as build dependency
  * Switch to github tarballs for including all required files for tests

 -- Pirate Praveen <praveen@debian.org>  Tue, 26 Oct 2021 00:51:17 +0530

ruby-fog-aws (3.9.0-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.6.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Fri, 27 Aug 2021 16:59:33 +0530

ruby-fog-aws (3.9.0-1) experimental; urgency=medium

  [ Abraham Raji ]
  * New upstream version 3.9.0
  * Bump watch file version to 4
  * Bump debhelper-compat version to 13
  * Update Forward information for patches

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Apr 2021 16:13:56 +0530

ruby-fog-aws (3.8.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ Pirate Praveen ]
  * New upstream version 3.8.0
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * Refresh patches
  * Disable more failing tests
  * Drop bin directory from binary (not useful)

 -- Pirate Praveen <praveen@debian.org>  Sun, 07 Feb 2021 17:16:04 +0530

ruby-fog-aws (3.5.2-1) unstable; urgency=medium

  * New upstream version 3.5.2
  * Add salsa-ci.yml
  * Refresh d/patches
  * Update d/control wrt cme
  * Add myself as an uploader

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Wed, 02 Oct 2019 19:27:51 +0530

ruby-fog-aws (3.3.0-5) unstable; urgency=medium

  * Remove Testsuite: autopkgtest-pkg-ruby (it does not process debian/clean)

 -- Pirate Praveen <praveen@debian.org>  Mon, 11 Feb 2019 09:55:12 +0530

ruby-fog-aws (3.3.0-4) unstable; urgency=medium

  [ Sruthi Chandran ]
  * Refresh patch
  * Add patch to remove relative path to lib in tests to fix autopkgtest
  * Remove coverage directory in clean

  [ Pirate Praveen ]
  * Remove test files that need ruby-fog-core 2.1.2 (fog-core 2.1.2 breaks
    ruby-fog-google. Upstream has an unmerged pull request, but that is not
    merged yet, because it breaks compatibility)

 -- Pirate Praveen <praveen@debian.org>  Sun, 10 Feb 2019 22:18:13 +0530

ruby-fog-aws (3.3.0-3) unstable; urgency=medium

  * Remove Breaks: gitlab (gitlab not in testing)

 -- Pirate Praveen <praveen@debian.org>  Fri, 08 Feb 2019 15:53:30 +0530

ruby-fog-aws (3.3.0-2) unstable; urgency=medium

  * Team upload
  * Reupload to unstable

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Thu, 07 Feb 2019 15:20:30 +0530

ruby-fog-aws (3.3.0-1) experimental; urgency=medium

  [ Utkarsh Gupta ]
  * Add patch description
  * Update d/copyright and d/control
  * Update d/rules
  * Add d/upstream/metadata
  * Add binaries in d/source/include-binaries
  * Add <!nocheck> for ruby-fog-core

  [ Manas Kashyap ]
  * New upstream release
  * Bump standard version
  * watch file updated
  * Patches updated

  [ Pirate Praveen ]
  * Ignore test failures
  * Add Rules-Requires-Root: no

 -- Pirate Praveen <praveen@debian.org>  Thu, 07 Feb 2019 12:35:39 +0530

ruby-fog-aws (2.0.1-1) unstable; urgency=medium

  * New upstream version 2.0.1
  * Bump Standards-Version to 4.1.4 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields

 -- Pirate Praveen <praveen@debian.org>  Mon, 23 Apr 2018 15:31:22 +0530

ruby-fog-aws (2.0.0-3) unstable; urgency=medium

  * Add Breaks+Replaces: ruby-fog (<< 1.42.0) (Closes: #884514)

 -- Pirate Praveen <praveen@debian.org>  Sat, 03 Mar 2018 01:29:41 +0530

ruby-fog-aws (2.0.0-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump debhelper compat to 11, standards version to 4.1.3

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Feb 2018 20:13:49 +0530

ruby-fog-aws (2.0.0-1) experimental; urgency=medium

  * New upstream release
  * Remove patch: 02-use-rubyzip-1.0-apis-for-tests.patch (applied upstream)

 -- Pirate Praveen <praveen@debian.org>  Fri, 15 Dec 2017 21:25:39 +0530

ruby-fog-aws (1.4.1-1) experimental; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sun, 10 Sep 2017 21:50:19 +0530

ruby-fog-aws (0.12.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * debian/control:
    - Add versioned build-depends on ruby-excon
    - Bump Standards-Version

 -- Micah Anderson <micah@debian.org>  Tue, 15 Nov 2016 09:13:18 -0500

ruby-fog-aws (0.9.2-1) unstable; urgency=medium

  * New upstream release
  * Add patch 04-disable-aws-connectivity-test.patch (disable failing tests)

 -- Pirate Praveen <praveen@debian.org>  Mon, 13 Jun 2016 20:38:07 +0530

ruby-fog-aws (0.7.6-1) unstable; urgency=medium

  * New upstream release.
  * Wrap ruby-fog-aws binary package dependencies list.
  * Add patch d/p/03-workaround-aws-dns-service-tests.patch.

 -- Miguel Landaeta <nomadium@debian.org>  Sun, 27 Sep 2015 00:26:39 -0300

ruby-fog-aws (0.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop patch 03-escape-lambda-function-names-in-requests-path.patch, it was
    merged at upstream.

 -- Miguel Landaeta <nomadium@debian.org>  Tue, 07 Jul 2015 17:26:08 -0300

ruby-fog-aws (0.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Team upload.
  * Add myself to Uploaders.
  * Bump DH compat level to 9.
  * Enable unit tests.
    - Update Build-Depends.
    - Add patch 01-dont-use-rubygems-to-run-tests.patch.
    - Add patch 02-use-rubyzip-1.0-apis-for-tests.patch.

 -- Miguel Landaeta <nomadium@debian.org>  Fri, 03 Jul 2015 12:42:31 -0300

ruby-fog-aws (0.1.2-2) unstable; urgency=medium

  * Re-upload to unstable

 -- Pirate Praveen <praveen@debian.org>  Wed, 13 May 2015 11:45:00 +0530

ruby-fog-aws (0.1.2-1) experimental; urgency=medium

  * Initial release (Closes: #782295)

 -- Pirate Praveen <praveen@debian.org>  Fri, 10 Apr 2015 17:15:50 +0530
