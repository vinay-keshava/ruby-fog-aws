require 'gem2deb/rake/testtask'

task :default => :test

# Failing tests are removed in debian/clean. TODO: figure out a way to exclude
#  them in this file
mock = ENV['FOG_MOCK'] || 'true'
task :test do
  sh("export FOG_MOCK=#{mock} && #{RbConfig.ruby} /usr/bin/shindont")
end
